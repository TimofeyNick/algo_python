def backward(x, A):
    ans = []
    len_lis = max(x)
    for j in range(len(A)):
        if x[j] == len_lis:
            len_lis -= 1
            ans.append(A[j])
    return ans


def lis(A):
    a = len(A)
    x = [1] * a
    for i in reversed(range(a)):
        for j in range(i, a):
            if A[j] > A[i]:
                x[i] = max(x[i], 1 + x[j])
    return backward(x, A)