def WFI(adj_matrix):
    res = [[adj_matrix[i][j] for j in range(len(adj_matrix))] for i in range(len(adj_matrix))]
    for k in range(len(adj_matrix)):
        for i in range(len(adj_matrix)):
            for j in range(len(adj_matrix)):
                res[i][j] = min(res[i][j], res[i][k] + res[k][j])

    return res



