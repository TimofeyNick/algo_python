def parent_ind(i):
    return (i - 1) // 2


def left_ind(i):
    return 2 * i + 1


def right_ind(i):
    return 2 * i + 2


def max_heapify(array, i, heap_len=None):
    if heap_len is None:
        heap_len = len(array)
    l = left_ind(i)
    r = right_ind(i)
    largest = i
    if l < heap_len and array[l] > array[i]:
        largest = l
    if r < heap_len and array[r] > array[largest]:
        largest = r
    if i != largest:
        array[i], array[largest] = array[largest], array[i]
        max_heapify(array, largest, heap_len)


def build_max_heap(array):
    for i in reversed(range(len(array) // 2)):
        max_heapify(array, i)
    return array


def heapsort(array):
    heap = build_max_heap(array)
    print(*heap)
    heap_len = len(heap)
    for i in reversed(range(1, len(heap))):
        heap[0], heap[i] = heap[i], heap[0]
        heap_len = heap_len - 1
        max_heapify(heap, 0, heap_len)
        print(*heap)


# n = int(input())
# array = list(map(int, input().split()))
# array = [8, 6, 1, 5, 2]
# array = [6, 7, 3]
# heap = build_max_heap(array)
# print(*heap)
heapsort([3, 2, 5, 0, -1])
