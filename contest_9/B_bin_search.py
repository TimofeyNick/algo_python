def binary_search(A, x, l=0, r=None):
    if r is None: r = len(A) - 1
    if r < l:
        return -1
    q = (r + l) // 2
    if A[q] == x:
        return q + 1
    elif A[q] > x:
        r = q - 1
        return binary_search(A, x, l, r)
    elif A[q] < x:
        l = q + 1
        return binary_search(A, x, l, r)

A = list(map(int, input().split()))
x = int(input())
print(binary_search(A, x))