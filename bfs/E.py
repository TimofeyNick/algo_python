from collections import deque


LETTERS = 'abcdefgh'
NUMBERS = '12345678'


def add_edge(v1, v2, adj_dict):
    adj_dict[v1].add(v2)
    adj_dict[v2].add(v1)


def place(i, j):
    return LETTERS[i] + NUMBERS[j]


def generate_adj_dict():
    adj_dict = dict()
    for l in LETTERS:
        for n in NUMBERS:
            adj_dict[l+n] = set()
    for i in range(8):
        for j in range(8):
            if 0 <= i + 2 < 8 and 0 <= j + 1 < 8:
                add_edge(place(i, j), place(i+2, j+1), adj_dict)
            if 0 <= i + 1 < 8 and 0 <= j + 2 < 8:
                add_edge(place(i, j), place(i+1, j+2), adj_dict)
            if 0 <= i - 1 < 8 and 0 <= j + 2 < 8:
                add_edge(place(i, j), place(i-1, j+2), adj_dict)
            if 0 <= i + 2 < 8 and 0 <= j - 1 < 8:
                add_edge(place(i, j), place(i+2, j-1), adj_dict)
            if 0 <= i - 2 < 8 and 0 <= j + 1 < 8:
                add_edge(place(i, j), place(i-2, j+1), adj_dict)
            if 0 <= i + 1 < 8 and 0 <= j - 2 < 8:
                add_edge(place(i, j), place(i+1, j-2), adj_dict)
            if 0 <= i - 2 < 8 and 0 <= j - 1 < 8:
                add_edge(place(i, j), place(i-2, j-1), adj_dict)
            if 0 <= i - 1 < 8 and 0 <= j - 2 < 8:
                add_edge(place(i, j), place(i-1, j-2), adj_dict)
    return adj_dict

#TODO
