def read_adj_list():
    n, m = map(int, input().split())
    adj_list = [[] for _ in range(n)]
    for _ in range(m):
        start, finish = map(int, input().split())
        adj_list[start].append(finish)
    return adj_list


def dfs(curr_node, adj_list, black_set, grey_set=None):
    if grey_set is None:
        grey_set = set()
    if black_set is None:
        black_set = set()
    grey_set.add(curr_node)
    for adj_node in adj_list[curr_node]:
        if adj_node in grey_set:  
            return [adj_node, curr_node]
        if adj_node not in black_set:
            tmp = dfs(adj_node, adj_list, black_set, grey_set)
            if len(tmp) > 0:
                if tmp[0] != tmp[-1]:
                    tmp.append(curr_node)
                return tmp
    grey_set.discard(curr_node)
    black_set.add(curr_node)
    return []


def search_for_cycle(adj_list):
    visited = set()
    for i in range(len(adj_list)):
        if i not in visited:
            res = dfs(i, adj_list, visited)
            if len(res) > 0:
                res.pop()
                return ' '.join(map(str, res[::-1]))
    return 'YES'


adj_list = read_adj_list()
print(search_for_cycle(adj_list))

