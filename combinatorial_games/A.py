def game(N, moves, answers):
    if N not in answers:
        move_1, move_2 = moves[N % 3]

        if N < move_1:
            answers[N] = False
        elif N < move_2:
            answers[N] = not game(N - move_1, moves, answers)
        else:
            answers[N] = not (game(N - move_1, moves, answers) and game(N - move_2, moves, answers))

    return answers[N]


