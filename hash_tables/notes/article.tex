\documentclass[12pt]{article}
\usepackage[a4paper, paperwidth=17.2cm, paperheight=25cm, inner=1.5cm, top=1.5cm, outer=1cm, bottom=1cm, includefoot]{geometry}
\usepackage{amsmath}
\usepackage{bm}
\usepackage[max2]{authblk}
\usepackage{amssymb}
\usepackage{graphicx}
\newcommand{\bx}{{\bm x}}
\newcommand{\kb}{k_{\rm B}}
\newcommand{\<}{\langle}
\renewcommand{\>}{\rangle}
\newcommand{\calL}{{\mathcal L}} % caligraphic L
\usepackage{amsmath}


% commenting
\usepackage{color}
\definecolor{ascol}{rgb}{0.7,0, 0}
%\newcommand{\commentas}[1]{{\color{ascol} \footnotesize \it [\marginpar{\bfseries !}AS: #1]}}
%\newcommand{\commentep}[1]{{\color{epcol} \footnotesize \it [\marginpar{\bfseries !}EP: #1]}}
\newcommand{\commentas}[1]{{\color{ascol} \footnotesize \it [AS: #1]}}
\newcommand{\alert}[1]{{\color{red}  #1 }}
%\renewcommand{\baselinestretch}{1.5}

\DeclareMathOperator*{\sumint}{%
	\mathchoice%
	{\ooalign{$\displaystyle\sum$\cr\hidewidth$\displaystyle\int$\hidewidth\cr}}
	{\ooalign{\raisebox{.14\height}{\scalebox{.7}{$\textstyle\sum$}}\cr\hidewidth$\textstyle\int$\hidewidth\cr}}
	{\ooalign{\raisebox{.2\height}{\scalebox{.6}{$\scriptstyle\sum$}}\cr$\scriptstyle\int$\cr}}
	{\ooalign{\raisebox{.2\height}{\scalebox{.6}{$\scriptstyle\sum$}}\cr$\scriptstyle\int$\cr}}
}

\newcommand{\cfg}{{\rm cfg}}

\begin{document}

\title{Hashes, Rabin-Karp Algorithm}
\author{Timofey Miryashkin}

\maketitle

\subsection*{Horner's Rule}
$A(x) = \sum_{j=0}^{n-1} a_j x^j$. Takes $\theta(n)$ time.
\[
A(x_0) = a_0 + x (a_1 + x(a_2 + \ldots  x_0 (a_{n-2} + x_0 (a_{n-1})) \ldots ))
\]

\subsection*{Hash Function Example}
Assume $str$ is the string of numbers $\{0, 1, 2, ..., 9\}$. $\text{Len}(str) = m$.  
\begin{align*}
 h(str) =&  
 \sum_{j=1}^{m} d^{m-j} str[j] \\
 =& d^{m-1} str[1] + d^{m-2} str[2] + \ldots + str[m]
\end{align*}
Assume $d=3$, $s=3141$:
\begin{align*}
h(3145) &= 3^3 \cdot 3 + 3^2 \cdot  1 + 3^1 \cdot  4 +  3^0 \cdot  5 \\
&= 107
\end{align*}
 Assume s is the string of characters. 
 \[
 h(\text{str}) =   \sum_{j=1}^{m} d^{m-j} \text{ord}(str[j]) 
 \]


\subsection*{The Rabin-Karp Algorithm}
Algorithm for string matching that requires $\Theta(n)$ time. \\
Given a pattern $P[1...m]$, $p =  h_1(P[1...m])$.\\ 
Given a text $T[1...n]$, $u_s = h_1(T[s+1, ..., s+m])$, $s = 0, 1, ..., n-m$. 
\[
 h_1(str) =  
 \sum_{j=1}^{m} d^{m-j} str[j]
 \]

\[
u_{s+1} = d (u_{s} - d^{m-1} T[s+1] ) + T[s+m+1]
\]
But the numbers $u_s$ may be way too big. We use better hash function -- \textbf{polynomial hash function}. We denote $t_s = h_2(T[s+1, ..., s+m])$.

\begin{equation}
h_2(str) =  
 \sum_{j=1}^{m} d^{m-j} str[j] \;\; \text{mod} \; q
\end{equation}
But how do we recalculate $t_{s+1}$ using $t_s$? \\
 We denote $ h = d^{m-1} ( \text{mod} \; q)$
 \[
 t_{s+1} = (d (t_s - T[s+1] h) + T[s+m+1]) \;\; \text{mod} \; q
 \]
\textbf{Properties}. To show that this is true, one should use the following properties: 
 \begin{align*}
[u_1 u_2 + c] \; \text{mod} \; q &=  [u_1 \;  \text{mod} \; q \cdot u_2 + c] \; \text{mod} \; q \\
[u_1 u_2 + c] \; \text{mod} \; q &=  [u_1 u_2 + c\; \text{mod} \; q ] \; \text{mod} \; q
 \end{align*}
\textit{Proof}. Here we prove the first property. We calculate $r_1$: $ r_1 = u_1  \; \text{mod} \; q $. So, we use representation of $u_1$ in the form: $u_1 = q k_1 + r_1$.
\begin{align*}
[u_1 u_2 + c] \; \text{mod} \; q &= [q k_1 u_2 + r_1 u_2 + c] \; \text{mod} \; q \\
&= [r_1 u_2 + c] \; \text{mod} \; q \\
&= [u_1 \;  \text{mod} \; q \cdot u_2 + c] \; \text{mod} \; q
\end{align*}
\textbf{Lemma}. Now we will prove the equation for $t_{s+1}$. \\
\textit{Proof}.
\begin{align*}
t_s &= u_s \; \text{mod} \; q \\ 
t_{s+1} &= u_{s+1} \; \text{mod} \; q
\end{align*}
\begin{align*}
t_{s+1} &= u_{s+1} \; \text{mod} \; q \\ 
&= d (u_{s} - d^{m-1} T[s+1] ) + T[s+m+1]  \; \; \text{mod} \; q \\
&= d \cdot (u_{s} - d^{m-1} T[s+1] ) \; \text{mod} \; q + T[s+m+1] \; \; \text{mod} \; q \\ 
&= d \cdot (t_{s} - d^{m-1} \; \text{mod} \; q \; \cdot T[s+1] ) \; \text{mod} \; q + T[s+m+1] \; \; \text{mod} \; q \\
&= d \cdot (t_{s} - h T[s+1] ) \; \text{mod} \; q + T[s+m+1] \; \; \text{mod} \; q \\ 
&= d  (t_{s} - h T[s+1] ) + T[s+m+1] \; \; \text{mod} \; q
\end{align*}
\newpage
\subsubsection*{Pseudocode}
 \begin{figure}[h!]
 	\begin{center}
 	\includegraphics[width=11.5cm]{pseudocode.jpg}
 	\end{center}
 \end{figure}


\end{document}
