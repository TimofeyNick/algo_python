def backward(x, A, B):
    i = j = 0
    ans = []
    while i < len(A) and j < len(B):
        if x[i][j] == x[i+1][j]:
            i += 1
        elif x[i][j+1] == x[i][j]:
            j += 1
        else:
            ans.append(A[i])
            i += 1
            j += 1
    return ans


def lcs(A, B):
    a, b = len(A), len(B)
    x = [[0] * (b + 1) for _ in range(a + 1)]
    for i in reversed(range(a)):
        for j in reversed(range(b)):
            if A[i] == B[j]:
                x[i][j] = x[i + 1][j + 1] + 1
            else:
                x[i][j] = max(x[i + 1][j], x[i][j + 1])
    
    return backward(x, A, B)


# A = [1, 5, 7, 8, 9, 2, 4, 6]
# B = [3, 6, 7, 5, 7, 6, 1, 2]
# print(lcs(A, B))
