class Task1:

    def __init__(self, french, pianists, swimmers):
        self.french = french
        self.pianists = pianists
        self.swimmers = swimmers

    def special_students(self):
        """swimmers-pianists not learning French"""
        # TODO
        return None


class Task2:

    def __init__(self, list_1, list_2):
        self.list_1 = list_1
        self.list_2 = list_2

    def get_unique_list_1(self):
        """return unique elements of the first list"""
        # TODO
        return None

    def get_unique_both_lists(self):
        """return unique elements of the both lists"""
        # TODO
        return None


class Task3:

    def __init__(self, films):
        self.films = films

    def get_results(self):
        """return dict with counts"""
        # TODO
        return None


class Task4:

    def __init__(self, text):
        self.text = text

    def word_counter(self):
        """word counter dict in descending order"""
        # TODO
        pass


class Task5:

    def __init__(self, family):
        self.family = family

    def parents(self, name):
        """ returns list of parents """
        # TODO
        pass

    def grandparents(self, name):
        """ returns list of grandmothers and grandfathers """
        # TODO
        pass

    def siblings(self, name):
        """ returns list of siblings """
        # TODO
        pass

    def children(self, name):
        """ returns list of children """
        # TODO
        pass

    def grandchildren(self, name):
        """ returns list of grandchildren """
        # TODO
        pass
