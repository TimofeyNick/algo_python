def pol_hash(string, base, mod):
    pol = 0
    for char in string:
        pol = (base*pol + ord(char))
    return pol % mod

print(pol_hash(string="abc", base=3, mod=1000))
