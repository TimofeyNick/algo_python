def F(i):    
    if i < 2:
        memo[i] = 1
    elif memo[i] is None:
        memo[i] = F(i - 1) + F(i - 2)
    return memo[i]

n = int(input())
memo = [None for i in range(n+1)]
print(F(n))