from collections import deque


def read_adj_list(n_vertices, m_edges):
    adj_list = [[] for _ in range(n_vertices)]
    for _ in range(m_edges):
        start, finish = map(int, input().split())
        adj_list[start].append(finish)
        adj_list[finish].append(start)
    return adj_list


def bfs(start_node, adj_list):
    parents = dict()
    dists = dict()
    queue = deque()
    queue.appendleft(start_node)
    dists[start_node] = 0
    while len(queue) > 0:
        curr_node = queue.pop()
        for adj_node in adj_list[curr_node]:
            if adj_node not in dists:
                dists[adj_node] = dists[curr_node] + 1
                parents[adj_node] = curr_node
                queue.appendleft(adj_node)
    return parents, dists


n, m, start_node, searched_node = map(int, input().split())
adj_list = read_adj_list(n, m)
parents, dists = bfs(start_node, adj_list)
print(dists[searched_node])
