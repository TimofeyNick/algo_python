def read_input():
    A = []
    for i in range(int(input())):
        A_subarray = []
        for j in range(int(input())):
            elem = input().split()
            A_subarray.append([float(elem[0]), elem[1]])
        A.append(A_subarray)
    return A

def merge(L, R):
    out = []
    i = j = 0
    while i < len(L) and j < len(R):
        if L[i][0] >= R[j][0]:
            out.append(L[i])
            i += 1
        else:
            out.append(R[j])
            j += 1
    out += L[i:] + R[j:]
    return out

def merge_rooms(A):
    while len(A) >= 2:
        A_new = []
        for i in range(0, len(A)//2):
            A_new.append(merge(A[2*i], A[2*i+1]))
        A_new += A[len(A)-len(A)%2:]
        A = A_new
    return A

def print_output(A):
    print(len(A[0]))
    for elem in A[0]:
        print("{:.2f} {}".format(elem[0], elem[1]))

# A = [[[909.94,'Savior'],[439.51, 'tywok']],
      # [[867.15, 'Ying'], [448.12, 'natori']],
      # [[500, 'LimberG'], [470, 'angsa']],
      # [[700, 'aubergineanode'], [200, 'shalinmangar']], 
      # [[630, 'aubergineanode'], [190, 'shalinmangar']]]

A = read_input()
A = merge_rooms(A)
print_output(A)

        