n = int(input())
color = list(map(int, input().split()))
paint = list(map(int, input().split()))
memo = [0 for i in range(len(color))]
memo[0] = 1
for i in range(len(color)-1):
    if color[i] == color[i+1] or paint[i] == color[i+1]:
        memo[i+1] += memo[i]
    if (i + 2 < n) and (color[i] == color[i+2] or paint[i] == paint[i+2]):
        memo[i+2] += memo[i]
        
print(memo[-1] % 947)
        