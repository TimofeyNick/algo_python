n = int(input())
path = [int(input()) for i in range(n)]
memo = [0 for i in range(n)]
for i in range(1, n):
    way1 = abs(path[i-1]-path[i]) + memo[i-1]
    if i - 2 >= 0:
        way2 = 3*abs(path[i-2]-path[i]) + memo[i-2]
    else:
        way2 = float('inf')
    memo[i] = min(way1, way2)

print(memo[-1])
