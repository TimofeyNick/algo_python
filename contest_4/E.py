n = int(input())
bottom = 0
top = round((n ** (1 / 3)))
while bottom <= top:
    test = bottom ** 3 + top ** 3
    if test > n:
        top -= 1 
    elif test < n:
        bottom += 1
    else:
        print(bottom, top)
        break
else:
    print('impossible')