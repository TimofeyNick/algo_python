def Grandi(N, K, answers):
    if N not in answers:
        if N == 0:
            answers[N] = 0
        else:
            child_grandi = {Grandi(N - i, K, answers) for i in range(1, min(N, K) + 1)}
            for i in range(10**10):
                if i not in child_grandi:
                    answers[N] = i
                    break
    return answers[N]
