def levenstein(A, B):
    a, b = len(A), len(B)
    x = [[0] * (b + 1) for _ in range(a + 1)]
    for i in range(a):
        x[i][-1] = a - i
    for i in range(b):
        x[-1][i] = b - i
    for i in reversed(range(a)):
        for j in reversed(range(b)):
            if A[i] == B[j]:
                x[i][j] = x[i + 1][j + 1]
            else:
                x[i][j] = min(x[i+1][j] + 1, x[i][j+1] + 1,  x[i+1][j+1] + 1)
    return x[0][0]


print(levenstein(input(), input()))

