class Node:
    """Node of a binary search tree.
    """
    def __init__(self, *, key=None, left=None, right=None, parent=None):
        self.key = key
        self.left = left
        self.right = right
        self.parent = parent


class BinaryTree:

    def __init__(self):
        self.root = None

    def insert_key(self, key):
        """Inserts the key in the binary search tree
        """
        z = Node(key=key)
        y = None
        x = self.root
        while x is not None:
            y = x
            if z.key <= x.key:
                x = x.left
            else:
                x = x.right
        if y is None:
            self.root = z
        else:
            z.parent = y
            if z.key < y.key:
                y.left = z
            else:
                y.right = z

    def insert_keys(self, keys: list):
        assert type(keys) == list
        for key in keys:
            self.insert_key(key)

    def inorder_walk(self, x: Node):
        """Returns keys in order
        """
        if x is None:
            return []
        return self.inorder_walk(x.left) + [x.key] + self.inorder_walk(x.right)

    def get_tree_height(self, x: Node):
        """Returns the height of the tree
        """
        # TODO

    def get_leaves(self, x: Node):
        """Returns the leaves of the tree
        """
        # TODO


tree = BinaryTree()
tree.insert_keys([35, 8, 6, 48, 30, 17, 4, 43, 42])
# Check that the tree data structure is working correctly
print(*tree.inorder_walk(tree.root))
# Task B: write get_tree_height() method
print(tree.get_tree_height(tree.root))
# Task C: write get_leaves() method
print(*tree.get_leaves(tree.root))
