def solve(sum, n):
    if n*9 < sum:
        return 'impossible'
    if n > sum:
        return 'impossible'
    # количество 9-ток
    num_9 = (sum-n) // 8
    # цифра перед 9-ками
    num_before_9 = sum - n - num_9*8
    # количество единиц
    num_1 = n - 1 - num_9
    out = ''
    if num_9 > 0:
        out += '9'*num_9
    if num_before_9 > 0:
        out = str(num_before_9+1) + out
    out = '1'*num_1 + out 
    return out     
    
sum, n = map(int, input().split())
print(solve(sum, n))