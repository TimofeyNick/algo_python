from collections import deque


def read_adj_list(n_vertices, m_edges):
    adj_list = [[] for _ in range(n_vertices)]
    for _ in range(m_edges):
        start, finish = map(int, input().split())
        adj_list[start].append(finish)
    return adj_list


def bfs(start_node, adj_list):
    parents = dict()
    dists = dict()
    queue = deque()
    queue.appendleft(start_node)
    dists[start_node] = 0
    while len(queue) > 0:
        curr_node = queue.pop()
        for adj_node in adj_list[curr_node]:
            if adj_node == start_node:
                cycle = [curr_node]
                while curr_node != start_node:
                    curr_node = parents[curr_node]
                    cycle.append(curr_node)
                return cycle[::-1]
            if adj_node not in dists:
                dists[adj_node] = dists[curr_node] + 1
                parents[adj_node] = curr_node
                queue.appendleft(adj_node)
    return None


def find_min_cycle(adj_list):
    min_cycle = None
    for node in range(len(adj_list)):
        cycle = bfs(node, adj_list)
        if cycle is not None:
            if min_cycle is None:
                min_cycle = cycle
            if len(min_cycle) > len(cycle):
                min_cycle = cycle
    if min_cycle is None:
        return 'NO CYCLES'
    else:
        return ' '.join(map(str, min_cycle))


n, m = map(int, input().split())
adj_list = read_adj_list(n, m)
print(find_min_cycle(adj_list))
