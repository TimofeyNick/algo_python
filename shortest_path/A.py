def input_graph(n, m):
    adj_list = [dict() for i in range(n)]
    for i in range(m):
        node_1, node_2, dist = map(int, input().split())
        adj_list[node_1][node_2] = dist
        adj_list[node_2][node_1] = dist
    return adj_list


def min_index(arr, excluded_set):
    min_v = float("inf")
    min_i = -1

    for i in range(len(arr)):
        if i not in excluded_set and arr[i] < min_v:
            min_i = i
            min_v = arr[i]

    return min_i


def dijkstra(adj_list, start_node, finish_node):
    distances = [0 if i == start_node else float("inf") for i in range(len(adj_list))]
    reached = set()

    while finish_node not in reached:
        curr_node = min_index(distances, reached)
        reached.add(curr_node)

        for adj_node in adj_list[curr_node]:
            distances[adj_node] = min(distances[adj_node], distances[curr_node] + adj_list[curr_node][adj_node])

    way = [finish_node]
    curr_node = finish_node

    while curr_node != start_node:
        for adj_node in adj_list[curr_node]:
            if distances[adj_node] == distances[curr_node] - adj_list[adj_node][curr_node]:
                curr_node = adj_node
                way.append(curr_node)
                break
    return way[::-1]


def solve_task():
    n, m, s, f = map(int, input().split())
    adj_list = input_graph(n, m)
    res = dijkstra(adj_list, s, f)
    print(*res)


solve_task()


















