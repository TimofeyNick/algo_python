import numpy as np


pairs = [(1, 2), (2, 1), (-1, 2), (2, -1), (-2, 1), (1, -2), (-2, -1), (-1, -2)]
rhos = []
for pair1 in pairs:
    x1, y1 = pair1
    for pair2 in pairs:
        x2, y2 = pair2
        rho = abs(x1-x2) + abs(y1-y2)
        rhos.append(rho)

print(np.unique(rhos))
