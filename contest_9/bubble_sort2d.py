def give_location(i, M):
    row = i // M 
    column = i - row*M
    return row, column

def print_matrix(A):
    for i in range(len(A)):
        print(*A[i])
    print()

def bubble_sort2d(A, N, M):
    len_matrix = N*M
    print_matrix(A)
    for outer_j in range(len_matrix- 1):
        for i in range(len_matrix - 1 - outer_j):
            icur_row, icur_col = give_location(i, M)
            inext_row, inext_col = give_location(i+1, M)
            if A[icur_row][icur_col] > A[inext_row][inext_col]:
                a_next = A[inext_row][inext_col]
                a_current = A[icur_row][icur_col]
                A[icur_row][icur_col], A[inext_row][inext_col] = a_next, a_current
                print_matrix(A)
                
bubble_sort2d([[0,3,4],[1,0,2]], 2, 3)