def read_adj_list():
    n = int(input())
    m = int(input())
    for _ in range(m):
        start, finish = map(int, input().split())
        adj_list[start].append(finish)
    return adj_list


def dfs(curr_node, adj_list, visited_nodes):
    visited_nodes.add(curr_node)
    for adj_node in adj_list[curr_node]:
        if adj_node not in visited_nodes:
            dfs(adj_node, adj_list, visited_nodes)
    return visited_nodes


visited_nodes = set()
adj_list = read_adj_list()
for i in range(len(adj_list)):
    if i not in visited:
        dfs(i, adj_list, visited_nodes)
