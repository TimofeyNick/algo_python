def binary_search(A, x):
    l = 0
    r = len(A) - 1
    while l <= r:
        q = (r + l) // 2
        if A[q] == x:
            # Because we return sequential number: q + 1
            return q + 1
        elif A[q] > x:
            r = q - 1
        elif A[q] < x:
            l = q + 1 
    # When r > l, we haven't found anything
    return -1

N, K = int(input()), int(input())
A = list(map(int, input().split()))
print(binary_search(A, K))
        
    