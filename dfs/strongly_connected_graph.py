def read_adj_list():
    n = int(input())
    m = int(input())
    for _ in range(m):
        start, finish = map(int, input().split())
        adj_list[start].append(finish)
    # adj_list = [[1], [2], [0]]
    # adj_list = [[1, 2], [2], []]
    # adj_list = [[1], [2], [1, 0]]
    return adj_list


def dfs(curr_node, adj_list, visited_nodes=None):
    if visited_nodes is None:
        visited_nodes = set()
    visited_nodes.add(curr_node)
    for adj_node in adj_list[curr_node]:
        if adj_node not in visited_nodes:
            dfs(adj_node, adj_list, visited_nodes)
    return visited_nodes


def reverse_adj_list(adj_list):
    res = [[] for i in range(len(adj_list))]
    for start in range(len(adj_list)):
        for finish in adj_list[start]:
            res[finish].append(start)
    return res


def check_strong_connection(adj_list):
    forward_pass = dfs(0, adj_list)
    backward_pass = dfs(0, reverse_adj_list(adj_list))
    if len(forward_pass) == len(adj_list) and forward_pass == backward_pass:
        return "YES"
    return "NO"


print(check_strong_connection(adj_list))
