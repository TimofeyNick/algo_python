def merge_sort(A, p = 0, r = None):
    if r is None:
        r = len(A)
    if 1 < r - p:
        q = (r + p + 1) // 2
        merge_sort(A, p, q)
        merge_sort(A, q, r)
        merge(A, p, q, r)
        
def merge(A, p, q, r):
    L = A[p:q]
    R = A[q:r]
    L.append(float('inf'))
    R.append(float('inf'))
    i = j = 0
    for k in range(p, r):
        if L[i] <= R[j]:
            A[k] = L[i]
            i += 1
        else:
            A[k] = R[j]
            j += 1
            
# A = [2, 4, 1, 3, 2, 10]
# merge_sort(A)
# print(A)