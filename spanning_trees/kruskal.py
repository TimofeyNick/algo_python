def find_index_in_set_list(index, set_list):
    for i in range(len(set_list)):
        if index in set_list[i]:
            return i


def kruskal(N, edges_list):
    res = []
    nodes_groups = [{i} for i in range(N)]

    edges_list.sort(key=lambda x: x[-1])

    for first_node, second_node, dist in edges_list:
        i = find_index_in_set_list(first_node, nodes_groups)
        j = find_index_in_set_list(second_node, nodes_groups)

        if i != j:
            nodes_groups[i] |= nodes_groups[j]
            nodes_groups.pop(j)

            res.append((first_node, second_node, dist))
            if len(nodes_groups) == 1:
                break

    return res


