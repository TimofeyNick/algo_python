import numpy as np

def partition_hoare(A, p, r):
    # x is the barrier element and is equal to A[p]
    x = A[p]
    i = p
    j = r
    while i < j:
        while j >= p and A[j] > x:
            j -= 1
        while i <= r and A[i] <= x:
            i += 1
        if i < j:
            A[j], A[i] = A[i], A[j]    
            j -= 1
            i += 1
        else:
            break
    # We put the barrier element A[p] in the right position
    A[j], A[p] = A[p], A[j]
    return j

def quick_sort(A, p=0, r=None):
    if r is None: 
        r = len(A) - 1
    if  p < r:
        q = partition_hoare(A, p, r)
        quick_sort(A, p, q-1)
        quick_sort(A, q+1, r)

A = np.random.randint(1, 10000, 15000)
quick_sort(A)
print('A is sorted:' , not np.sum(A != np.sort(A)))
