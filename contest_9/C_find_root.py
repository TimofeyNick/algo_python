def find_root(f, a, b, tol):
    while True:
        q = (b + a) / 2
        if abs(f(q)) < 1e-6:
            return q
        elif f(a)*f(q) > 0:
            a = q
        elif f(b)*f(q) > 0:
            b = q
    return q

f = lambda x: x + 1.1
print(find_root(f, -10, 10))
        