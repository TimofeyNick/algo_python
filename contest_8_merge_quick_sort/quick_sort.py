import numpy as np

def partition(A, p, r):
    q = p
    # A[r] is the barrier element
    for u in range(p, r):
        if A[u] <= A[r]:
            A[q], A[u] = A[u], A[q]
            q += 1
    # Put the barrier element A[r] in the right position
    A[q], A[r] = A[r], A[q] 
    return q
            

def quick_sort(A, p=0, r=None):
    if r is None:
        r = len(A) - 1
    if p < r:
        q = partition(A, p, r)
        quick_sort(A, p, q-1)
        quick_sort(A, q+1, r)

A = np.random.randint(1, 10000, 15000)
quick_sort(A)
print('A is sorted:' , not np.sum(A != np.sort(A)))