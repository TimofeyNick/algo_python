N = int(input())
data = list(map(int, input().split()))
ways = [0 for i in range(len(data))]
ways[0] = 1
for i in range(len(data)-1):
    ways[i+1] += ways[i]
    boost_index = i + data[i]    
    if data[i] > 1 and  boost_index < len(data):
            ways[boost_index] += ways[i]

print(ways[-1] % 937)
